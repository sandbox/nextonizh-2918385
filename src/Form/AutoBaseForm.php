<?php
/**
 * @file
 * Contains \Drupal\autobase\src\Form\AutoBaseForm.
 */
namespace Drupal\autobase\Form;

use Drupal\Core\Ajax\AfterCommand;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\autobase;

class AutoBaseForm extends FormBase {
  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return __CLASS__;
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $brend = $form_state->getValue('brend');
    $year = $form_state->getValue('year');
    $model = $form_state->getValue('model');
    $engine = $form_state->getValue('engine');

    $cached = ['brend' => '', 'year' => '', 'model' => '', 'engine' => ''];
    if ($storage = $form_state->getStorage()) {
      $cached = array_merge($cached, $storage['storage']);
    }

    $is_changed_brend = $cached['brend'] != $brend;
    $is_changed_year = $cached['year'] != $year;
    $is_changed_model = $cached['model'] != $model;
    $hide_engine = $is_changed_brend || $is_changed_year;

    $form['#prefix'] = '<div id="wrapper-autobaseform">';
    $form['#suffix'] = '</div>';


    $ajax_settings = [
      '#ajax' => [
        'callback' => '::ajaxAutoBaseFormCallback',
        'event' => 'change',
        'wrapper' => 'wrapper-autobaseform',
      ],
      '#attributes' =>[
        'class' =>['form-control'],
      ]
    ];

    $complete_form = &$form_state->getCompleteForm();
    $brend_options = isset($complete_form['brend']['#options']) ? $complete_form['brend']['#options'] : autobase_brend_options();
    $form['brend'] = [
        '#type' => 'select',
        '#options' => array('' => 'Выберите Марку') + $brend_options,
      ] + $ajax_settings;

    if (!empty($brend)) {
      $form_state->set(['storage', 'brend'], $brend);
      $year_options = (isset($complete_form['year']['#options']) && !$is_changed_brend) ? $complete_form['year']['#options'] : autobase_year_options($brend);
      $form['year'] = [
          '#type' => 'select',
          '#options' => array('' => 'Выберите Год') + $year_options,
        ] + $ajax_settings;
    }

    if (!empty($year) && !$is_changed_brend) {
      $form_state->set(['storage', 'year'], $year);
      $model_options = (isset($complete_form['model']['#options']) && !$is_changed_year) ? $complete_form['model']['#options'] : autobase_model_options($brend, $year);
      $form['model'] = [
          '#type' => 'select',
          '#options' => array('' => 'Выберите Модель') + $model_options,
        ] + $ajax_settings;
    }

    if (!empty($model) && !$hide_engine) {
      $form_state->set(['storage', 'model'], $model);
      $engine_options = autobase_engine_options($brend, $year, $model);
      $form['engine'] = [
          '#type' => 'select',
          '#options' =>  array('' => 'Выберите Модификацию') + $engine_options,
        ] + $ajax_settings;
    }

    foreach(array_reverse(array_keys($cached)) as $select_field){
      if (isset($form[$select_field])){
        $form[$select_field]['#attributes']['class'][] ='field-current';
        break;
      }
    }



    if (!empty($engine) && !$is_changed_model  && !$is_changed_brend  && !$is_changed_year) {
      $tsizes = autobase_tire_sizes($engine);
      $wsizes = autobase_wheels_sizes($engine);
      if ($wsizes) {
        $lz = $wsizes['f'][0]->lz;
        $lz2 = $wsizes['f'][0]->wheels_lz;
        $pcd = $wsizes['f'][0]->pcd;
        $pcd2 = $wsizes['f'][0]->wheels_pcd;
        $dia = $wsizes['f'][0]->wheels_dia;
        $bolt = $wsizes['f'][0]->wheels_bolt;
        $gaika = $wsizes['f'][0]->wheels_gaika;
      }

      $result_content = [
        '#theme' => 'autobasesizes',
        '#brend' => $brend_options[$brend],
        '#year' => $year_options[$year],
        '#model' => $model_options[$model],
        '#engine' => $engine_options[$engine],
        '#tsizes' => $tsizes,
        '#wsizes' => $wsizes,
        '#lz2' => $lz2,
        '#pcd2' => $pcd2,
        '#lz' => $lz,
        '#pcd' => $pcd,
        '#dia' => $dia,
        '#bolt' => $bolt,
        '#gaika' => $gaika,
      ];

      $html = \Drupal::service('renderer')->renderRoot($result_content);
      $form['#suffix'] = '<div id="wrapper-autobaseresults">' . $html . '</div></div>';
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function ajaxAutoBaseFormCallback(array &$form, FormStateInterface $form_state) {
    $form_state->setRebuild(TRUE);
    return $form;
  }


  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    drupal_set_message($this->t('Your form has been submitted.'));
  }
}