<?php

namespace Drupal\autobase\Plugin\Block;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Provides a block with a simple text.
 *
 * @Block(
 *   id = "autobaseform",
 *   admin_label = @Translation("Auto Base Form"),
 * )
 */
class AutoBaseFormBlock extends BlockBase {
  /**
   * {@inheritdoc}
   */
  public function build() {
    return  \Drupal::formBuilder()->getForm('\Drupal\autobase\Form\AutoBaseForm');
  }

  /**
   * {@inheritdoc}
   */
  protected function blockAccess(AccountInterface $account) {
    return AccessResult::allowedIfHasPermission($account, 'access content');
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $config = $this->getConfiguration();
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->configuration['autobaseform_settings'] = $form_state->getValue('autobaseform_settings');
  }
}